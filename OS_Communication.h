//********************************************************************************
/*!
\author     Kraemer E
\date       30.01.2019

\file       HelperFunctions.h
\brief      Helper functions for the message handler

***********************************************************************************/

#ifndef _HELPERFUNCTIONS_H_
#define _HELPERFUNCTIONS_H_

  
      
/********************************* includes **********************************/
#include "OS_Config.h"
#ifdef USE_OS_COMMUNICATION
    
#include "OS_Messages.h"
    
/***************************** defines / macros ******************************/
#define OS_COMMUNICATION_FILL_CMD(CMD,ID,TYPE)  sMsgFrame.sPayload.ucCommand = (CMD);\
                                                sMsgFrame.sPayload.ucMsgId = (ID);\
                                                sMsgFrame.sHeader.ucMsgType = (TYPE);
/****************************** type definitions *****************************/

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif  

void    OS_Communication_Tick(u8 ucElapsedMs);
void    OS_Communication_SendAcknowledge(const teMessageId eMsgId, const u8 ucQueryID, const teMessageType eResponse);
void    OS_Communication_SendResponseMessage(const teMessageId eMsgID, const void* pvMsgData, const u8 ucMsgDataLen, const teMessageCmd eCommand);
void    OS_Communication_SendRequestMessage(const teMessageId eMsgID, const void* pvMsgData, const u8 ucMsgDataLen, const teMessageCmd eCommand);
void    OS_Communication_ResponseReceived(const tsMessageFrame* psMsgFrame);

u8    OS_Communication_CreateMessageSpecFrame(tsMessageFrame* psMsgFrame, u8 ucDest, u8 ucSource, u8* pucPayload, u8 ucPayloadLength);
u8    OS_Communication_CreateMessageFrame(tsMessageFrame* psMsgFrame, u8* pucPayload, u8 ucPayloadLength);
u32     OS_Communication_CreateMessageCrc(u8* pucBuffer, u8 ucSize);

u8              OS_Communication_GetFrameSize(const tsMessageFrame* psMsgFrame);
u8              OS_Communication_GetMessageSizeWithoutCrc(const tsMessageFrame* psMsgFrame);
u8              OS_Communication_GetSourceAddress(const tsMessageFrame* psMsgFrame);
u8              OS_Communication_GetDestinationAddress(const tsMessageFrame* psMsgFrame);
teMessageId     OS_Communication_GetObject(const tsMessageFrame* psMsgFrame);
teMessageCmd    OS_Communication_GetCommand(const tsMessageFrame* psMsgFrame);
teMessageType   OS_Communication_GetMessageType(const tsMessageFrame* psMsgFrame);

bool    OS_Communication_ValidateMessageAddresses(const tsMessageFrame* psMsgFrame);

void    OS_Communication_HandleSerialCommEvent(void);
void    OS_Communication_Init(pFunctionParamVoid pvMessageHandler);
void    OS_Communication_SendDebugMessage(char* pucMsg);

extern void MessageHandler_HandleMessage(void* pvMsg);

#endif

#ifdef __cplusplus
}
#endif    

#endif //_HELPERFUNCTIONS_H_
