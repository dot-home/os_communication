//********************************************************************************
/*!
 \author     Kraemer E.
 \date       05.02.2019

 \file       Serial.c
 \brief      Serial Uart communication functions

 ***********************************************************************************/

/********************************* includes **********************************/
#include "OS_Serial_UART.h"
#if USE_OS_SERIAL_UART
#include <string.h>

#include "BaseTypes.h"
#include "HAL_Serial_UART.h"
#include "HAL_System.h"
#include "OS_FIFO.h"

#include "OS_SoftwareTimer.h"
#include "OS_Messages.h"
#include "OS_EventManager.h"

/***************************** defines / macros ******************************/    
#define START_FLAG      0xA5    //0b1010 0101
#define ESCAPE_FLAG     0x7E    //0b0111 1110
#define END_FLAG        0x66    //0b0110 0110
    
#define LENGTH_BYTE_CORRECTION    1
#define FORCE_SERIAL_COMM_ERRORS  0
#define MESSAGE_BUFFER           10
    
#define TX_BUFFER_SIZE  250
#define RX_BUFFER_SIZE  250
    
//Calculate the receive timeout: Size of the msg frage Header(4x u8) + Payload (8x u8) + CRC (4x u8) = 16x u8 plus a Startbit and a Stopbit for each
// -->128bit + 32bit = 160bit (Data-bits)
// ReceiveTime = Databits/Baudrate @38400 = 4.2ms timeout
#define MILLISECOND_FACTOR  1000
#define RECEIVE_TIMEOUT     (((sizeof(tsMessageFrame)*10*MILLISECOND_FACTOR)/ 38400)*2)
    
/************************ local data type definitions ************************/
typedef enum
{
    eReceiveStart,
    eReceiveData
} teReceiveState;

typedef struct
{
    u8 ucByteCount;
    u8 ucStartIdx;
    u8 ucSize;
    bool bSavedMsg;
    bool bResponseReceived;
} tsMsgStruct;

typedef struct
{
    tsMsgStruct sMessages[MESSAGE_BUFFER];
    tsFIFO* psFifoStructure;
    u8 ucMsgPutIdx;
    u8 ucMsgGetIdx;
    u8 ucMsgCounter;
} tsMsgBuffer;

/************************* local function prototypes *************************/
static void ReceiveTimeout(void);
static void CallbackDataOnRx(u8 ucData);
static void CallbackTxComplete(void);


/************************* local data (const and var) ************************/
static u8 TxBuffer[TX_BUFFER_SIZE];
static u8 RxBuffer[RX_BUFFER_SIZE];

static tsFIFO sTxFifo;
static tsFIFO sRxFifo;

static tsMsgBuffer sMsgBufferTx;
static tsMsgBuffer sMsgBufferRx;

static u8 ucReceiveTimeoutMax = INVALID_TIMER_INDEX;

static bool bTransmitComplete = true;

/* Use state machine for decoding messages */
static teReceiveState eState = eReceiveStart;
static bool bEscapeFlagReceived = false;


/************************ export data (const and var) ************************/

/****************************** local functions ******************************/
//********************************************************************************
/*!
 \author     Kraemer E
 \date       08.04.2020
 \brief      Callback for a receive timeout. The received data is discarded.
 \return     none
 \param      none
 ***********************************************************************************/
static void ReceiveTimeout(void)
{
    /* Delete message frame and send a fault message out */
    /* Disable receive interrupt while loading the buffer */
    //HAL_Serial_UART_EnableRxIrq(false);
    HAL_System_EnterCriticalSection();

    tsMsgStruct* psMsg = &sMsgBufferRx.sMessages[sMsgBufferRx.ucMsgPutIdx];
    
    /* Remove the previously received data */
    OS_FIFO_RemovePutValues(sMsgBufferRx.psFifoStructure, psMsg->ucSize);

    /* Reset message structure  */
    psMsg->bSavedMsg = false;
    psMsg->ucByteCount = 0;
    psMsg->ucSize = 0;
    
    if(sMsgBufferRx.ucMsgCounter)
        --sMsgBufferRx.ucMsgCounter;
    
    /* Change state machine for decoding messages */
    eState = eReceiveStart;
    bEscapeFlagReceived = false;
    
    /* Enable the interrupt again */
    //HAL_Serial_UART_EnableRxIrq(true);
    HAL_System_LeaveCriticalSection();;
}


//********************************************************************************
/*!
 \author     Kraemer E
 \date       27.01.2019
 \brief      Callback for RX interrupt
 \return     none
 \param      ucData - Data from the UART RX register
 ***********************************************************************************/
static void CallbackDataOnRx(u8 ucData)
{
    /* Check first if message buffer isn't full */
    if(sMsgBufferRx.ucMsgCounter <= MESSAGE_BUFFER)
    {
        /* Use pointer for easier handling */
        tsMsgStruct* pMsgRx = &sMsgBufferRx.sMessages[sMsgBufferRx.ucMsgPutIdx];
        
        switch(eState)
        {
            /* Check for prefix or start flag */
            case eReceiveStart:
            {
                /* Switch to next state when valid otherwise start again */
                if(ucData == START_FLAG)
                {
                    eState = eReceiveData;

                    /* Enable receive timeout control */
                    OS_SW_Timer_SetTimerState(ucReceiveTimeoutMax, eSwTimer_StatusRunning);
                }
                break;
            }

                /* Save message values */
            case eReceiveData:
            {
                /* Check for "escape-flag" */
                if(ucData == ESCAPE_FLAG && bEscapeFlagReceived == false)
                {
                    /* Next message is equal to a known flag (start, stop, or escape) */
                    bEscapeFlagReceived = true;
                    
                    /* Don't put anything in the buffer */
                }
                else
                {
                    /* When the first data arrives for this new message */
                    if(pMsgRx->bSavedMsg == false)
                    {
                        /* New message started */
                        pMsgRx->bSavedMsg = true;

                        /* Save message start index */
                        pMsgRx->ucStartIdx = sMsgBufferRx.psFifoStructure->ucPutIdx;
                    }
                    
                    /* Check for End flag */
                    if(ucData == END_FLAG && bEscapeFlagReceived == false)
                    {
                        /* Disable receive timeout control */
                        OS_SW_Timer_SetTimerState(ucReceiveTimeoutMax, eSwTimer_StatusSuspended);
                        
                        /* Increment put index */
                        sMsgBufferRx.ucMsgPutIdx++;
                        
                        /* When the end of the message buffer is reached */
                        if(sMsgBufferRx.ucMsgPutIdx == MESSAGE_BUFFER)
                        {
                            /* Put it back to the start */
                            sMsgBufferRx.ucMsgPutIdx = 0;
                        }
                        
                        /* Increment msg counter */
                        sMsgBufferRx.ucMsgCounter++;
                        
                        /* Valid message received. Post event */
                        OS_EVT_PostEvent(eEvtSerialMsgReceived, 0, 0);
                        
                        /* Message is done, so start the state machine again */
                        eState = eReceiveStart;
                    }
                    else
                    {
                        /* Put into fifo buffer */
                        teFifoResult eFifoPut = OS_FIFO_Put(sMsgBufferRx.psFifoStructure, ucData);
                        
                        /* Increment byte counter */
                        pMsgRx->ucByteCount++;
                        
                        /* Increment saved size */
                        if(eFifoPut == eFIFO_OK)
                        {
                            pMsgRx->ucSize++;
                        }
                    }
                    
                    /* Next message is regular data */
                    bEscapeFlagReceived = false;
                }
                break;
            }

            default:
                break;
        }
    }
}

//********************************************************************************
/*!
 \author     Kraemer E.
 \date       05.02.2019
 \fn         CallbackTxComplete
 \brief      Callback for tx complete interrupt
 \return     none
 ***********************************************************************************/
static void CallbackTxComplete(void)
{
    if(bTransmitComplete == false)
    {
        /* Check if end of the message is reached */
        if(sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgGetIdx].ucByteCount)
        {
            u8 ucData = 0;
            
            /* Get the next byte */
            OS_FIFO_Get(sMsgBufferTx.psFifoStructure, &ucData);
            
            /* Send next byte */
            HAL_Serial_UART_WriteByte(ucData);
            
            /* Decrement byte count */
            --sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgGetIdx].ucByteCount;
            
            bTransmitComplete = false;

        }
        /* Transmition is completed */
        else
        {
            /* Decrement messages and increment get index */
            --sMsgBufferTx.ucMsgCounter;
            sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgGetIdx].bSavedMsg = false;
            sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgGetIdx].ucByteCount = 0;
            sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgGetIdx].ucSize = 0;
            
            /* Increment the get counter */
            ++sMsgBufferTx.ucMsgGetIdx;
            
            /* When the end of the message buffer is reached */
            if(sMsgBufferTx.ucMsgGetIdx == MESSAGE_BUFFER)
            {
                sMsgBufferTx.ucMsgGetIdx = 0;
            }
            
            /* Release transmit complete */
            bTransmitComplete = true;
            
            /* Put event */
            OS_EVT_PostEvent(eEvtSerialMsgSend, sMsgBufferTx.ucMsgCounter, 0);
        }
    }
}


/****************************** extern functions ******************************/
//********************************************************************************
/*!
 \author     Kraemer E.
 \date       05.02.2019
 \fn         Serial_TransmitMessage
 \brief      Transmit a message over the UART
 \return     void
 \param      pBuffer  - buffer pointer
 \param      ucCount  - number of bytes in buffer
 ***********************************************************************************/
bool OS_Serial_UART_TransmitMessage(void)
{
    bool bSend = false;

    /* Check if there are messages left */
    if(sMsgBufferTx.ucMsgCounter)
    {
        /* Start a new message transmission only when the last one is done */
        if(bTransmitComplete == true)
        {
            /* Set the get index which message from the buffer shall be send */
            u8 ucGetIdx = sMsgBufferTx.ucMsgGetIdx;
            
            /* Get the message size */
            u8 ucMsgSize = sMsgBufferTx.sMessages[ucGetIdx].ucSize;
            
            /* Save the message size for the byte-count. The byte-counter will
             be decremented for each byte which is send */
            sMsgBufferTx.sMessages[ucGetIdx].ucByteCount = ucMsgSize;

            /* Disable UART isr first */
            //HAL_Serial_UART_EnableTxIrq(false);
            //HAL_Serial_UART_EnableRxIrq(false);
            HAL_System_EnterCriticalSection();
            
            u8 ucDataStart = 0;
            
            /* Start with the first byte */
            OS_FIFO_Get(sMsgBufferTx.psFifoStructure, &ucDataStart);

            /* Set transmit info to false */
            bTransmitComplete = false;
            
            /* Enable TX isr again */
            //HAL_Serial_UART_EnableTxIrq(true);
            //HAL_Serial_UART_EnableRxIrq(true);
            HAL_System_LeaveCriticalSection();
            
            /* Start sending */
            HAL_Serial_UART_WriteByte(ucDataStart);
            
            /* Decrement send info */
            sMsgBufferTx.sMessages[ucGetIdx].ucByteCount--;

            #ifdef ESPRESSIF_ESP8266
                //No TX-Interrupt available. Use while loop.
                while(bTransmitComplete == false)
                {
                    CallbackTxComplete();
                }
            #endif

        }
    }

    return bSend;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       27.01.2019
 \fn         SerialComm_Init
 \brief      Initalizes FIFOs and link them to the message buffer structures.
 \return     none
 ***********************************************************************************/
void OS_Serial_UART_Init(void)
{
    /* Create FIFOs when they are already exist, clear them */
    OS_FIFO_Create(sizeof(TxBuffer), &sTxFifo, &TxBuffer[0]);
    OS_FIFO_Create(sizeof(RxBuffer), &sRxFifo, &RxBuffer[0]);

    /* Clear message structures */
    memset(&sMsgBufferRx, 0, sizeof(sMsgBufferRx));
    memset(&sMsgBufferTx, 0, sizeof(sMsgBufferTx));

    /* Connect the buffer with the message structures */
    sMsgBufferRx.psFifoStructure = &sRxFifo;
    sMsgBufferTx.psFifoStructure = &sTxFifo;

    /* Set callbacks for serial hardware events */
    HAL_Serial_UART_Init(CallbackDataOnRx, CallbackTxComplete);
    
    /* Create an asynchron timer for the receive timeout */
    OS_SW_Timer_CreateAsyncTimer(&ucReceiveTimeoutMax, (RECEIVE_TIMEOUT), eSwTimer_CreateSuspended, ReceiveTimeout);
    
    bTransmitComplete = true;
}

//********************************************************************************
/*!
 \author     Kraemer
 \date       05.02.2019
 \fn         Serial_SendPacket
 \brief      This function encodes the messages and put it into the TX-Buffer
 \return     ucMsgIndexInBuffer - The count of the index in the message buffer.
             0xFF is an invalid put
 \param      pucMsg   - pointer to message
 \param      ucCount   - length of packet
 ***********************************************************************************/
u8 OS_Serial_UART_SendPacket(const u8* pucMsg, u8 ucCount)
{
    bool bSuccessfullPut = false;
    u8 ucMsgIndexInBuffer = 0xFF;
    
    /* Check if message is allowed in buffer */
    if(sMsgBufferTx.ucMsgCounter < MESSAGE_BUFFER)
    {
        /* Check for valid pointer first */
        if(pucMsg && ucCount)
        {
            /* Save message in message buffer */
            if(sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].bSavedMsg == false)
            {
                sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].ucStartIdx = sMsgBufferTx.psFifoStructure->ucPutIdx;
                sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].bSavedMsg = true;
                sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].ucSize = ucCount;
            }
            
            /* Put start flag first */
            OS_FIFO_Put(sMsgBufferTx.psFifoStructure, START_FLAG);
            sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].ucSize++;
            
            /* Put into buffer */
            while(ucCount--)
            {
                /* Check for start, escape and stop flags */
                if(*pucMsg == START_FLAG || *pucMsg == END_FLAG || *pucMsg == ESCAPE_FLAG)
                {
                    /* One flag was found, put an escape flag into buffer */
                    OS_FIFO_Put(sMsgBufferTx.psFifoStructure, ESCAPE_FLAG);
                    sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].ucSize++;
                }
                
                /* Put data into fifo buffer */
                OS_FIFO_Put(sMsgBufferTx.psFifoStructure, *pucMsg);
                
                /* Increment buffer index */
                ++pucMsg;
            }
            
            /* Put end flag at least */
            OS_FIFO_Put(sMsgBufferTx.psFifoStructure, END_FLAG);
            sMsgBufferTx.sMessages[sMsgBufferTx.ucMsgPutIdx].ucSize++;
            
            bSuccessfullPut = true;
        }

        /* When data is in buffer, start the transmission */
        if(bSuccessfullPut)
        {
            /* Save return value for valid put */
            ucMsgIndexInBuffer = sMsgBufferTx.ucMsgPutIdx;
            
            /* Increment for the next message in the buffer */
            sMsgBufferTx.ucMsgCounter++;
            sMsgBufferTx.ucMsgPutIdx++;
            
            if(sMsgBufferTx.ucMsgPutIdx == MESSAGE_BUFFER)
            {
                sMsgBufferTx.ucMsgPutIdx = 0;
            }
            
            /* Send message */
            OS_Serial_UART_TransmitMessage();
        }
    }
    
    return ucMsgIndexInBuffer;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       30.01.2019
 \fn         Serial_GetPacket
 \brief      Checks whether there is a packet and returns it to the caller.
 in case the pBuffer is a NULLPOINTER and pCount is a valid pointer
 only the message size is written to where pCount is pointing at
 \return     bReturnVal - true if packet has been retrieved
 \param      pucBuffer  - Pointer to the given buffer
 \param      pucCount   - size of return buffer
 ***********************************************************************************/
bool OS_Serial_UART_GetPacket(u8* pucBuffer, u8* pucCount)
{
    bool bReturnVal = false;

    /* Check for valid pointer first */
    if(pucBuffer && pucCount)
    {
        /* Check if there is any message in the receive buffer */
        if(sMsgBufferRx.ucMsgCounter)
        {
            u8 ucGetIdx = sMsgBufferRx.ucMsgGetIdx;
            u8 ucMsgSize = sMsgBufferRx.sMessages[ucGetIdx].ucSize;
            
            /* Check if message size is portable into buffer */
            if(*pucCount >= ucMsgSize)
            {
                /* Disable receive interrupt while loading the buffer */
                //HAL_Serial_UART_EnableTxIrq(false);
                //HAL_Serial_UART_EnableRxIrq(false);
                HAL_System_EnterCriticalSection();
                
                /* Get the message */
                OS_FIFO_GetBuffer(sMsgBufferRx.psFifoStructure, pucBuffer, ucMsgSize, true);
                
                /* Enable the interrupt again */
                //HAL_Serial_UART_EnableRxIrq(true);
                //HAL_Serial_UART_EnableTxIrq(true);
                HAL_System_LeaveCriticalSection();
                
                /* Decrement messages and increment get index */
                --sMsgBufferRx.ucMsgCounter;
                sMsgBufferRx.sMessages[ucGetIdx].bSavedMsg = false;
                sMsgBufferRx.sMessages[ucGetIdx].ucByteCount = 0;
                sMsgBufferRx.sMessages[ucGetIdx].ucSize = 0;
                ++sMsgBufferRx.ucMsgGetIdx;
                
                /* When the end of the message buffer is reached */
                if(sMsgBufferRx.ucMsgGetIdx == MESSAGE_BUFFER)
                {
                    sMsgBufferRx.ucMsgGetIdx = 0;
                }
                
                bReturnVal = true;
            }
        }
    }
    return bReturnVal;
}

//********************************************************************************
/*!
 \author     Kraemer E.
 \date       20.02.2019
 \fn         Serial_EnableUartWakeupInSleep
 \brief      Puts the UART into a sleep mode where the RX line serves as a wake up
 source for the deep sleep.
 \param      none
 \return     none
 ***********************************************************************************/
bool OS_Serial_UART_EnableUartWakeupInSleep(void)
{
    bool bChangeToWakeUpSrc = false;
    /* Check for empty RX and TX buffer */
    //TODO: Check for messages which are currently put
    if(1)
    {
        /* Put UART in wake up source mode */
        HAL_Serial_UART_EnableUartWakeup();
        bChangeToWakeUpSrc = true;
    }
    
    return bChangeToWakeUpSrc;
}

//********************************************************************************
/*!
 \author     Kraemer E.
 \date       20.02.2019
 \fn         Serial_DisableUartWakeupInSleep
 \brief      Disables the UART wake up source for the deep sleep.
 \param      none
 \return     none
 ***********************************************************************************/
void OS_Serial_UART_DisableUartWakeupInSleep(void)
{
    /* Disable wake up mode */
    HAL_Serial_UART_DisableUartWakeup();
}

//********************************************************************************
/*!
 \author     Kraemer E.
 \date       22.05.2019
 \fn         Serial_TransmitStatus
 \brief      Returns the actual transmit status.
 \return     bTransmitComplete - True when transmission is done
 ***********************************************************************************/
bool OS_Serial_UART_TransmitStatus(void)
{
    return bTransmitComplete;
}

//********************************************************************************
/*!
 \author     Kraemer E.
 \date       27.06.2021
 \brief      Callback interface for HAL access
 \return     none
 ***********************************************************************************/
void OS_Serial_UART_PollRxData(void)
{
    HAL_Serial_UART_PollRxData();
}

//********************************************************************************
/*!
 \author     Kraemer E.
 \date       14.07.2021
 \brief      Prints the given string literal on the UART
 \param      szString - The string literal
 \return     none
 ***********************************************************************************/
void OS_Serial_UART_Printf(char* szString)
{
    HAL_Serial_UART_Printf(szString);
}

#endif //USE_OS_SERIAL_UART