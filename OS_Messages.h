//********************************************************************************
/*!
\author     Kraemer E
\date       30.01.2019

\file       Messages.h
\brief      Messages for communication

***********************************************************************************/
#ifndef _PCM_MESSAGES_H_
#define _PCM_MESSAGES_H_

#include "BaseTypes.h"

#ifdef __cplusplus
extern "C"
{
#endif    


/************************************ Defines **********************************/
#define ADDRESS_MASTER      0x10
#define ADDRESS_SLAVE1      0x01
#define PREAMBE             0x1C

#define DATA_MAX_SIZE       255u
#define INVALID_QUERY_ID    255u

#define MAX_OUTPUTS         4


/************************************ Message Ids **********************************/
/***        Enum                    |  ID |      Size in bytes              |       Description     **/
typedef enum
{
    eMsgRequestOutputStatus = 0x01,  /*<-- Request for output values. Slave => Master        --> */                  	
    eMsgUpdateOutputStatus  = 0x02,  /*<-- Updated output values. Slave <= Master            --> */                 
    eMsgVersion             = 0x03,  /*<-- Message for Software Version.  Slave <= Master    --> */                 
    eMsgInitOutputStatus    = 0x04,  /*<-- Message with the output values from the FLASH.  Slave <= Master    --> */
    eMsgErrorCode           = 0x05,  /*<-- Fault code message. Currently unknown handling    --> */                 
    eMsgSleep               = 0x06,  /*<-- Sleep request. Slave <= Master                    --> */                 
    eMsgWakeUp              = 0x07,  /*<-- Wake up request. Slave <=> Master                 --> */                 
    eMsgAutoInitHardware    = 0x08,  /*<-- Automatic Hardware init request. Slave => Master  --> */                 
    eMsgManualInitHardware  = 0x09,  /*<-- Message with the Min-Max-Values. Slave => Master  --> */                 
    eMsgManualInitHwDone    = 0x0A,  /*<-- Message to trigger saving of the system data. Slave => Master  --> */    
    eMsgUserTimer           = 0x0B,  /*<-- Message with the user timers. Slave <=> Master    --> */                 
    eMsgSystemStarted       = 0x0C,  /*<-- Slave started message. Slave => Master            --> */                 
    eMsgOutputMeasurement   = 0x0D,  /*<-- The current, voltage and temperature values. --> */
    eMsgCurrentTime         = 0x0E,  /*<-- The current time from the ethernet. Slave => Master  --> */              
    eMsgStillAlive          = 0x0F,  /*<-- Alive check message before the slave is reseted   --> */                 
    eMsgDebug               = 0x10,  /*<-- Debug message --> */                                                     
    eMsgInitDone            = 0x11,  /*<-- Message for user settings sending done -->*/                             
    eMsgGetOutputStatus     = 0x12,  /*<-- Message for output status requesting*/   
    eMsgEnableNightMode     = 0x13,  /*<-- Sets the night mode status. When the master is within the night time the output is set to MIN  --> */
    eMsgEnableMotionDetect  = 0x14,  /*<-- Sets the motion detection status. When its on the PIR input is checked to --> */
    eMsgEnableAutomaticMode = 0x15,  /*<-- Sets the automatic mode status. When the time is within the start-stop time the output is switched on --> */
    eMsgBurningTime         = 0x16,  /*<-- Get command: retrieves the current count of the burning time. Set command: sets the new value for the burning time*/
    eMsgInvalid
}teMessageId;

typedef enum
{
    eNoCmd  = 0x00,     /* No command */
    eCmdGet = 0x01,     /* Read command */
    eCmdSet = 0x02      /* Write command */
}teMessageCmd;

typedef enum
{
    eNoType        = 0x00,      /* No message type -> Invalid */
    eTypeRequest   = 0x01,      /* Request type. Expects a response */
    eTypeResponse  = 0x02,      /* Response type to a request */
    eTypeDenied    = 0x04,      /* Denied message */
    eTypeUnknown   = 0x08,      /* Unknown type ??? */
    eTypeAck       = 0x10,      /* Message received ACK (Request and response) */
    eTypeDebug     = 0x20       /* Debug message type */
}teMessageType;

#pragma pack(push, 1)

/********************************** Message frame **********************************/
typedef struct
{
    u8  ucPreamble;
    u8  ucDestAddress;
    u8  ucSourceAddress;
    u8  ucMsgType;
}tsFrameHeader;

//! Payload content. Wolf uses object and command inside of payload. Not our idea!
typedef struct
{
    u8  ucMsgId;    //Name of the message
    u8  ucCommand;  //Command for this message
    u8  ucQueryID;  //Just a counter ID which is incremented with each message
    u8  ucPayloadLen;
    u8* pucData;    //Pointer to the Data. Data is saved in the buffer
}tsPayload;

//! Format of whole message frame
typedef struct
{
    tsFrameHeader   sHeader;
    tsPayload       sPayload;
    u32             ulCrc32;
}tsMessageFrame;


/********************************** Message data **********************************/

//Response needed
typedef struct 
{
    u8 ucBrightness;              /* Requested brightness for this output */
    u8 ucLedStatus;               /* The requested status of the output (On / Off) */
    u8 ucInitMenuActive;          /* Shows if Initializing menu is active */
    u8 ucOutputIndex;             /* The which output shall be enabled */
}tMsgRequestOutputState;


typedef struct
{
    uint8_t ucOutputIdx;
}tsMsgGetOutputState_Req;

typedef struct
{
    uint8_t ucBrightness;
    uint8_t ucLedStatus;
    uint8_t ucOutputIndex;
}tsMsgGetOutputState_Res;

typedef struct
{
    struct
    {
        u8 ucBrightness;
        u8 ucLedStatus;
        u8 ucOutputIndex; 
    }asOutputs[MAX_OUTPUTS];
    
    u8 ucAutomaticModeActive;
    u8 ucMotionDetectionOnOff;
    u8 ucNightModeOnOff;
    u8 ucBurnTime;
}tMsgInitOutputState;


typedef struct _msg_update_output_state
{
    u8 ucBrightness;
    u8 ucLedStatus;
    
    u8 ucNightModeOnOff;
    u8 ucMotionDetectionOnOff;
    u8 ucAutomaticModeOnOff;
    u8 ucOutputIndex;    
    
    s32 slRemainingBurnTime;
}tMsgUpdateOutputState;


typedef struct 
{
    u32 ulVoltage;          /* Voltage in millivolt */
    u16 uiCurrent;          /* Current in milli ampere */
    s16 siTemperature;      
    u8  ucOutputIndex;
}tsMsgOutputMeasure_Res;

typedef struct 
{
    uint8_t  ucOutputIndex;
}tsMsgOutputMeasure_Req;

typedef struct
{
    u32 ulTicks;
    u8 ucHour;
    u8 ucMinutes;
}tMsgCurrentTime;

//response is version
typedef struct
{
    char* pucVersionBuffer; 
}tMsgVersion; 

//No response needed
typedef struct _msg_fault
{
    u16 uiErrorCode;
}tMsgFaultMessage;

typedef struct _msg_manual_init
{
    u8   ucSetMinValue;
    u8   ucSetMaxValue;
    u8   ucOutputIndex;
}tMsgManualInit;

typedef struct _msg_user_timer
{
    u8 ucStartHour; 
    u8 ucStopHour;
    u8 ucStartMin;
    u8 ucStopMin;
    u8 ucTimerIdx;
}tMsgUserTimer;


typedef struct _msg_stillalive
{
    u8  bResponse;
    u8  bRequest;
}tMsgStillAlive;

typedef struct
{
    u8* pucDebugMsg;
}tsMsgDebug;

typedef struct
{
    u8  ucNightModeStatus;      /* Night mode should be switched on or off */
}tsMsgEnableNightMode;

typedef struct
{
    u8  ucMotionDetectStatus;   /* PIR detection enabled or disabled */
    u8 ucBurnTime;              /* Requested burning time */
}tsMsgEnableMotionDetectStatus;

typedef struct
{
    u8  ucAutomaticModeStatus;  /* Shows if the automatic mode is enabled /disabled */
}tsMsgEnableAutomaticMode;

typedef struct 
{
    uint32_t ulBurningTime;
}tsMsgBurningTime;


#pragma pack(pop)

#ifdef __cplusplus
}
#endif    

#endif //_PCM_MESSAGES_H_
