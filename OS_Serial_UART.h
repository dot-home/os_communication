//********************************************************************************
/*!
\author     Kraemer E.
\date       05.02.2019

\file       Serial.c
\brief      Serial Uart communication functions

***********************************************************************************/
#ifndef _OS_SERIAL_UART_H_
#define _OS_SERIAL_UART_H_
    
#include "OS_Config.h"
#if USE_OS_SERIAL_UART
    
/********************************* includes **********************************/
#include "BaseTypes.h"
    
/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/
    
#ifdef __cplusplus
extern "C"
{
#endif

void OS_Serial_UART_Init (void);
u8   OS_Serial_UART_SendPacket(const u8* pucMsg, u8 ucCount);
bool OS_Serial_UART_GetPacket(u8* pucBuffer, u8* pucCount);
bool OS_Serial_UART_TransmitMessage(void);
bool OS_Serial_UART_EnableUartWakeupInSleep(void);
bool OS_Serial_UART_TransmitStatus(void);
void OS_Serial_UART_DisableUartWakeupInSleep(void);
void OS_Serial_UART_PollRxData(void);

void OS_Serial_UART_Printf(char* szString);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_SERIAL_UART

#endif // _OS_SERIAL_UART_H_
