//********************************************************************************
/*!
\author     Kraemer E
\date       30.01.2019

\file       MessageHandler.c
\brief      Handler for the serial communication messages

***********************************************************************************/
#include "OS_Communication.h"
#if USE_OS_COMMUNICATION

#include "BaseTypes.h"
#include "OS_Messages.h"
#include "OS_ErrorDebouncer.h"

#include "OS_CRC.h"
#include "OS_Serial_UART.h"
#include "OS_FIFO.h"
#include "OS_Memory.h"
#include "stdio.h"

//#include "Version\Version.h"
/****************************************** Defines ******************************************************/
#define INVALID_MESSAGES_MAX    10
#define SAVED_MESSAGES          20
#define RESEND_COUNTER_LIMIT    1
#define RESEND_TIMEOUT          500
#define RESEND_ENABLED          0
#if !RESEND_ENABLED
    #warning RESEND-Disabled!
#endif

#define COPY_BUFFER_INTO_FRAME  1

/****************************************** Variables ****************************************************/
typedef struct
{
    u32* pulMemoryAddress;
    uint16_t   ucReSendTimeout;
    u8   ucFrameLength;
    u8   ucQueryID;    
    u8   ucReSendCounter;
    bool bResponseReceived;    
}tsSavedMsgFrame;

typedef struct
{
    u8 ucSavedMsgCounter;
    tsSavedMsgFrame sSavedFrames[SAVED_MESSAGES];
}tsTransportControl;

static tsTransportControl sTransportCtrl;
static pFunctionParamVoid pFnMessageHandler = NULL;
static u8 ucGlobalQueryID = 0;
/****************************************** Function prototypes ******************************************/
static void ClearSavedMsgFrameEntry(tsSavedMsgFrame* psSaveFrame);
static void SaveMessageInResendBuffer(tsSavedMsgFrame* psSaveFrame, const u8* pucBuffer, const u8 ucFrameLength);
static void SendMessageFrame(tsMessageFrame* psMsgFrame, const void* pvMsgData, const u8 ucMsgDataLen);
static u8 CreateByteStreamFromMessageFrame(u8* pucBuffer, const tsMessageFrame* psMsgFrame);
#if COPY_BUFFER_INTO_FRAME
    static u8 CreateMessageFrameFromByteStream(u8* pucBuffer, tsMessageFrame* psMsgFrame);
#else
    static u8 LinkMessageFrameToByteStream(u8* pucBuffer, tsMessageFrame* psMsgFrame);
#endif
/****************************************** local functions *********************************************/
//********************************************************************************
/*!
\author     KraemerE
\date       07.07.2021
\brief      Clears the saved frame entry and initializes the entry again.
            also releases reserved memory.
\return     none
\param      psSaveFrame - Pointer where the resend-frame shall be linked to
***********************************************************************************/
static void ClearSavedMsgFrameEntry(tsSavedMsgFrame* psSaveFrame)
{
    /* Free memory first */
    teMemoryResult eRes = OS_Memory_Release(psSaveFrame->pulMemoryAddress, psSaveFrame->ucFrameLength);
    
    if(eRes == eMemory_OK)
    {    
        /* Clear the entry */
        memset(psSaveFrame, 0, sizeof(tsSavedMsgFrame));  
        
        /* Initialize the entry */
        psSaveFrame->ucQueryID = INVALID_QUERY_ID;
        psSaveFrame->pulMemoryAddress = NULL;
        
        if(sTransportCtrl.ucSavedMsgCounter)
            --sTransportCtrl.ucSavedMsgCounter;
    }
    else
    {
        OS_ErrorDebouncer_PutErrorInQueue(eOsComm_ResenBufferFault);
    }
}

//********************************************************************************
/*!
\author     KraemerE
\date       07.07.2021
\brief      Creates a bytestream from the given message frame.
\return     ucFrameBuffOffset - The used offset.
\param      psMsgFrame - The given message frame which shall be transcribed into a
                         bytestream.
***********************************************************************************/
static u8 CreateByteStreamFromMessageFrame(u8* pucBuffer, const tsMessageFrame* psMsgFrame)
{
    u8 ucFrameBuffOffset = 0;
    
    if(pucBuffer && psMsgFrame)
    {
        /* Copy frame header first */        
        memcpy(&pucBuffer[ucFrameBuffOffset], (u8*)&psMsgFrame->sHeader, sizeof(tsFrameHeader));
        ucFrameBuffOffset += sizeof(tsFrameHeader);
        
        /* Copy payload separately. Copy the payload without the data first. */
        u8 ucPayloadSizeWithoutDataPtr = sizeof(tsPayload) - sizeof(psMsgFrame->sPayload.pucData);
        memcpy(&pucBuffer[ucFrameBuffOffset], (u8*)&psMsgFrame->sPayload, ucPayloadSizeWithoutDataPtr);
        ucFrameBuffOffset += ucPayloadSizeWithoutDataPtr;
        
        /* Now copy the message data into the buffer. Empty payload cannot be copied */
        if(psMsgFrame->sPayload.pucData)
        {
            memcpy(&pucBuffer[ucFrameBuffOffset], (u8*)psMsgFrame->sPayload.pucData, psMsgFrame->sPayload.ucPayloadLen);
            ucFrameBuffOffset += psMsgFrame->sPayload.ucPayloadLen;
        }
        
        /* Copy CRC at least */
        memcpy(&pucBuffer[ucFrameBuffOffset], (u8*)&psMsgFrame->ulCrc32, sizeof(psMsgFrame->ulCrc32));
        ucFrameBuffOffset += sizeof(psMsgFrame->ulCrc32); 
    }
    
    return ucFrameBuffOffset;
}

#if COPY_BUFFER_INTO_FRAME
//********************************************************************************
/*!
\author     KraemerE
\date       09.07.2021
\brief      Creates a message frame from the given buffer.
\return     ucFrameBuffOffset - The used offset.
\param      pucBuffer - The byte stream which shall be used for copying the data into
                        the message frame.
\param      psMsgFrame - The given message frame which shall be transcribed from a
                         bytestream.
***********************************************************************************/
static u8 CreateMessageFrameFromByteStream(u8* pucBuffer, tsMessageFrame* psMsgFrame)
{
    u8 ucFrameBuffOffset = 0;
    
    if(pucBuffer && psMsgFrame)
    {
        /* Copy frame header first */
        memcpy((u8*)&psMsgFrame->sHeader, &pucBuffer[ucFrameBuffOffset], sizeof(tsFrameHeader));
        ucFrameBuffOffset += sizeof(tsFrameHeader);
        
        /* Copy payload separately. Copy the payload without the data first. */
        u8 ucPayloadSizeWithoutDataPtr = sizeof(tsPayload) - sizeof(psMsgFrame->sPayload.pucData);
        memcpy((u8*)&psMsgFrame->sPayload, &pucBuffer[ucFrameBuffOffset], ucPayloadSizeWithoutDataPtr);
        ucFrameBuffOffset += ucPayloadSizeWithoutDataPtr;
        
        /* Now copy the message data into the buffer. Empty payload cannot be copied */
        if(psMsgFrame->sPayload.ucPayloadLen)
        {
            psMsgFrame->sPayload.pucData = &pucBuffer[ucFrameBuffOffset];
            ucFrameBuffOffset += psMsgFrame->sPayload.ucPayloadLen;
        }
        
        /* Copy CRC at least */
        memcpy((u8*)&psMsgFrame->ulCrc32, &pucBuffer[ucFrameBuffOffset], sizeof(psMsgFrame->ulCrc32));
        ucFrameBuffOffset += sizeof(psMsgFrame->ulCrc32); 
    }
    
    return ucFrameBuffOffset;
}

#else
//********************************************************************************
/*!
\author     KraemerE
\date       09.07.2021
\brief      Links a message frame to the given buffer.
\return     ucFrameBuffOffset - The used offset.
\param      pucBuffer - The bytestream which shall be used for linking
\param      psMsgFrame - The given message frame which shall be linked into a
                         bytestream.
***********************************************************************************/
static u8 LinkMessageFrameToByteStream(u8* pucBuffer, tsMessageFrame* psMsgFrame)
{
    u8 ucFrameBuffOffset = 0;
    
    if(pucBuffer && psMsgFrame)
    {
        /* Link frame header first */
        psMsgFrame->sHeader = *(tsFrameHeader*)&pucBuffer[ucFrameBuffOffset];
        ucFrameBuffOffset += sizeof(tsFrameHeader);
        
        /* Link payload separately */
        u8 ucPayloadSizeWithoutDataPtr = sizeof(tsPayload) - sizeof(psMsgFrame->sPayload.pucData);
        psMsgFrame->sPayload = *(tsPayload*)&pucBuffer[ucFrameBuffOffset];
        ucFrameBuffOffset += ucPayloadSizeWithoutDataPtr;
        
        /* Link correct message data into the buffer. Empty payload cannot be linked */
        if(psMsgFrame->sHeader.ucPayloadLen)
        {
            psMsgFrame->sPayload.pucData = &pucBuffer[ucFrameBuffOffset];
            ucFrameBuffOffset += psMsgFrame->sHeader.ucPayloadLen;
        }
        else
        {
            psMsgFrame->sPayload.pucData = NULL;
        }
        
        /* Link CRC at least */
        psMsgFrame->ulCrc32 = *(u32*)&pucBuffer[ucFrameBuffOffset];
        ucFrameBuffOffset += sizeof(psMsgFrame->ulCrc32); 
    }
    
    return ucFrameBuffOffset;
}
#endif

//********************************************************************************
/*!
\author     KraemerE
\date       07.07.2021
\brief      Puts the message into resend heap buffer by spliting the frame into pieces.
            Message data is put instead of the Data pointer!
\return     none
\param      psSaveFrame - Pointer where the resend-frame shall be linked to
\param      pucBuffer - Message frame already saved as a byte stream
\param      ucFrameLength - The whole frame length (incl. correct message data)
***********************************************************************************/
static void SaveMessageInResendBuffer(tsSavedMsgFrame* psSaveFrame, const u8* pucBuffer, const u8 ucFrameLength)
{
    if(pucBuffer && psSaveFrame)
    {           
        /* Reserve heap memory for this frame */
        teMemoryResult eRes = OS_Memory_Create(&psSaveFrame->pulMemoryAddress, ucFrameLength);
        
        /* Save the length further readings */    
        psSaveFrame->ucFrameLength = ucFrameLength;
                
        /* Generate an error when the used size differs from the frame length */
        if(eRes != eMemory_OK)
        {
            OS_ErrorDebouncer_PutErrorInQueue(eOsComm_ResenBufferFault);
        }
        else
        {
            /* Save into heap */
            OS_Memory_PutBuffer(psSaveFrame->pulMemoryAddress, pucBuffer, ucFrameLength);
        }
    }
}


//********************************************************************************
/*!
\author     KraemerE
\date       07.07.2021
\brief      Puts the message into resend heap buffer by spliting the frame into pieces.
            Message data is put instead of the Data pointer!
\return     none
\param      psSaveFrame - Pointer where the resend-frame shall be linked to
\param      pvMsgData - Pointer to the message data (Message structure)
\param      ucMsgDataLen - The message data length
***********************************************************************************/
static void SendMessageFrame(tsMessageFrame* psMsgFrame, const void* pvMsgData, const u8 ucMsgDataLen)
{
    if(psMsgFrame)
    {        
        /* Create Message frame without CRC. Framelength includes the CRC size */
        u8 ucFrameLength = OS_Communication_CreateMessageFrame(psMsgFrame, (u8*)pvMsgData, ucMsgDataLen);
        
        /* Create bytestream from the frame */
        u8 aucTempBuffer[DATA_MAX_SIZE];
        u8 ucFrameBuffOffset = CreateByteStreamFromMessageFrame(&aucTempBuffer[0], psMsgFrame);
        
        /* Compare the converted length with the calculated length */
        if(ucFrameBuffOffset == ucFrameLength)
        {
            /* Get the message frame length without the CRC */
            u8 ucFrameWithoutCRC = OS_Communication_GetMessageSizeWithoutCrc(psMsgFrame);
            
            /* Calculate CRC. Use a pointer to overwrite the last bytes of the byte-stream (CRC bytes)*/
            //u32* pulPtr = (u32*)&aucTempBuffer[ucFrameWithoutCRC];
            u32 ulCRC = OS_Communication_CreateMessageCrc(&aucTempBuffer[0], ucFrameWithoutCRC);
            memcpy(&aucTempBuffer[ucFrameWithoutCRC], &ulCRC, sizeof(u32));            
        }
        
        /* Check if resend is necessary */
        if(psMsgFrame->sHeader.ucMsgType == eTypeRequest || psMsgFrame->sHeader.ucMsgType == eTypeResponse)
        {        
            tsSavedMsgFrame* psSaveFrame = NULL;
            
            /* Check for empty entries in the buffer */
            u8 ucBufferIdx;
            for(ucBufferIdx = 0; ucBufferIdx < _countof(sTransportCtrl.sSavedFrames); ucBufferIdx++)
            {
                if(sTransportCtrl.sSavedFrames[ucBufferIdx].ucQueryID == INVALID_QUERY_ID)
                {
                    //Emtpy entry found
                    psSaveFrame = &sTransportCtrl.sSavedFrames[ucBufferIdx];
                    break;
                }
            }
            
            /* Check if entry was found */
            if(psSaveFrame)
            {
                /* Put message ID into buffer */
                psSaveFrame->bResponseReceived = false;
                psSaveFrame->ucReSendCounter = RESEND_COUNTER_LIMIT;
                psSaveFrame->ucReSendTimeout = RESEND_TIMEOUT;
                psSaveFrame->ucQueryID = psMsgFrame->sPayload.ucQueryID;
                  
                /* Save the message into the resend buffer */
                SaveMessageInResendBuffer(psSaveFrame, &aucTempBuffer[0], ucFrameLength);

                ++sTransportCtrl.ucSavedMsgCounter;
            }
            else
            {
                /* No entry found. Generate a fault */
                OS_ErrorDebouncer_PutErrorInQueue(eOsComm_SendBufferFault);
            }
        }
        
        /* Send byte stream packed message */
        OS_Serial_UART_SendPacket((void*)&aucTempBuffer[0], ucFrameLength);       
    }
}

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     KraemerE
\date       10.04.2019
\brief      This handler saves the message in a buffer and starts the re-send timeout
            when the response won't come within the given time exact message is sent
            again.
\return     none
\param      sMsgFrame - The message frame
***********************************************************************************/
void OS_Communication_Init(pFunctionParamVoid pvMessageHandler)
{
    pFnMessageHandler = pvMessageHandler;     
    
    /* Init re-send buffer */
    u8 ucIdx = 0;
    for(ucIdx = 0; ucIdx < _countof(sTransportCtrl.sSavedFrames); ucIdx++)
    {
        sTransportCtrl.sSavedFrames[ucIdx].pulMemoryAddress = NULL;
        sTransportCtrl.sSavedFrames[ucIdx].bResponseReceived = true;
        sTransportCtrl.sSavedFrames[ucIdx].ucQueryID = INVALID_QUERY_ID;
    }
}

//********************************************************************************
/*!
\author     KraemerE    
\date       09.07.2021  
\brief      Creates a response message frame for the message data and sends it over
            the bus.
\return     none
\param      eMsgID       - The message ID
\param      pvMsgData    - Pointer to the given message structure
\param      ucMsgDataLen - The length of the message structure.
\param      eCommand     - Write or read request (Set or Get)
***********************************************************************************/
void OS_Communication_SendResponseMessage(const teMessageId eMsgID, const void* pvMsgData, const u8 ucMsgDataLen, const teMessageCmd eCommand)
{
    tsMessageFrame sMsgFrame;
    memset(&sMsgFrame, 0, sizeof(tsMessageFrame));    
    sMsgFrame.sPayload.ucQueryID = INVALID_QUERY_ID;
    
    /* Fill some header */
    OS_COMMUNICATION_FILL_CMD(eCommand, eMsgID, eTypeResponse)
    
    /* Put message into resend buffer and send over UART */
    SendMessageFrame(&sMsgFrame, pvMsgData, ucMsgDataLen);
}

//********************************************************************************
/*!
\author     KraemerE    
\date       09.07.2021  
\brief      Creates a request message frame for the message data and sends it over
            the bus.
\return     none
\param      eMsgID       - The message ID
\param      pvMsgData    - Pointer to the given message structure
\param      ucMsgDataLen - The length of the message structure.
\param      eCommand     - Write or read request (Set or Get)
***********************************************************************************/
void OS_Communication_SendRequestMessage(const teMessageId eMsgID, const void* pvMsgData, const u8 ucMsgDataLen, const teMessageCmd eCommand)
{
    tsMessageFrame sMsgFrame;
    memset(&sMsgFrame, 0, sizeof(tsMessageFrame));
    sMsgFrame.sPayload.ucQueryID = INVALID_QUERY_ID;
    
    /* Fill some header */
    OS_COMMUNICATION_FILL_CMD(eCommand, eMsgID, eTypeRequest)
    
    /* Put message into resend buffer and send over UART */
    SendMessageFrame(&sMsgFrame, pvMsgData, ucMsgDataLen);
}

//********************************************************************************
/*!
\author     KraemerE    
\date       09.07.2021  
\brief      Creates an ACK message frame for the message data and sends it over
            the bus.
\return     none
\param      eMsgID       - The message ID
\param      ucQueryID    - The query ID which shall be responsed
\param      eResponse    - The response type
***********************************************************************************/
void OS_Communication_SendAcknowledge(const teMessageId eMsgId, const u8 ucQueryID, const teMessageType eResponse)
{
    /* Create structure */
    tsMessageFrame sMsgFrame;
    memset(&sMsgFrame, 0, sizeof(sMsgFrame));
    sMsgFrame.sPayload.ucQueryID = ucQueryID;
    
    /* Fill them */
    OS_COMMUNICATION_FILL_CMD(eNoCmd, eMsgId, eResponse)
        
    /* Start to send the packet directly. Response frames shall not be saved again */
    SendMessageFrame(&sMsgFrame, NULL, 0);
}

//********************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      Function to create a debug-message frame and send it over the bus.
\return     none
\param      pucMsg - Message string which shall be send
***********************************************************************************/
void OS_Communication_SendDebugMessage(char* pucMsg)
{
    /* Create structure */
    tsMessageFrame sMsgFrame;
    
    /* Clear message structure */
    memset(&sMsgFrame, 0, sizeof(tsMessageFrame));
    
    /* Create pointer to get the size */
    char* ptr = pucMsg;
    u8 ucDebugMsgSize = 0;
    
    while(*ptr != 0)
    {
        if(ucDebugMsgSize < DATA_MAX_SIZE)
        {
            ++ucDebugMsgSize;
            ++ptr;
        }
        else
        {
            break;
        }
    }
    
    /* Fill command data */
    OS_COMMUNICATION_FILL_CMD(eNoCmd, eMsgDebug, eTypeDebug)
    
    /* Send message */
    SendMessageFrame(&sMsgFrame, pucMsg, ucDebugMsgSize);
}

//********************************************************************************
/*!
\author     KraemerE    
\date       10.04.2019  
\brief      This handler clears the message which is saved with this ID 
            from the buffer.
\return     none
\param      sMsgFrame - The message frame
***********************************************************************************/
void OS_Communication_ResponseReceived(const tsMessageFrame* psMsgFrame)
{
    tsSavedMsgFrame* psSaveFrame = NULL;
    
    /* Check for entry in the buffer */
    u8 ucBufferIdx;
    bool bMessageIdFound = false;
    for(ucBufferIdx = 0; ucBufferIdx < _countof(sTransportCtrl.sSavedFrames); ucBufferIdx++)
    {
        if(sTransportCtrl.sSavedFrames[ucBufferIdx].ucQueryID == psMsgFrame->sPayload.ucQueryID)
        {
            //Message found
            bMessageIdFound = true;
            psSaveFrame = &sTransportCtrl.sSavedFrames[ucBufferIdx];
            break;
        }
    }
    
    /* Check if message was found in the buffer */
    if(bMessageIdFound == false)
    {
        //TODO: Something is wrong here
    }
    else if(psSaveFrame)
    {
        ClearSavedMsgFrameEntry(psSaveFrame);
    }    
}


//********************************************************************************
/*!
\author     KraemerE    
\date       30.01.2019  
\brief      Returns the source address of the message.
\return     ucSourceAddress - The source of this message 
\param      sMsgFrame - The message frame
***********************************************************************************/
u8 OS_Communication_GetSourceAddress(const tsMessageFrame* psMsgFrame)
{
    return psMsgFrame->sHeader.ucSourceAddress;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       30.01.2019  
\brief      Returns the destination address of the message.
\return     ucDestAddress - The destination of this message 
\param      sMsgFrame - The message frame
***********************************************************************************/
u8 OS_Communication_GetDestinationAddress(const tsMessageFrame* psMsgFrame)
{
    return psMsgFrame->sHeader.ucDestAddress;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       30.01.2019  
\brief      Returns the object
\return     ucObject - Object or Message id
\param      sMsgFrame - The message frame
***********************************************************************************/
teMessageId OS_Communication_GetObject(const tsMessageFrame* psMsgFrame)
{
    return psMsgFrame->sPayload.ucMsgId;
}


//********************************************************************************
/*!
\author     KraemerE    
\date       30.01.2019  
\brief      Returns the command
\return     ucCommand - The Command id
\param      sMsgFrame - The message frame
***********************************************************************************/
teMessageCmd OS_Communication_GetCommand(const tsMessageFrame* psMsgFrame)
{
    return psMsgFrame->sPayload.ucCommand;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       07.04.2019  
\brief      Returns the message type
\return     teMessageType - The message type id
\param      sMsgFrame - The message frame
***********************************************************************************/
teMessageType OS_Communication_GetMessageType(const tsMessageFrame* psMsgFrame)
{
    return psMsgFrame->sHeader.ucMsgType;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       07.07.2021  
\brief      Returns the message length
\return     ucPayloadLen - The payload length
\param      sMsgFrame - The message frame
***********************************************************************************/
teMessageType OS_Communication_GetMessageLength(const tsMessageFrame* psMsgFrame)
{
    return psMsgFrame->sPayload.ucPayloadLen;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       17.02.2019  
\brief      Creates a CRC for the message which includes the header and the payload
\return     ucCommand - The Command id
\param      pucBuffer - The pointer to the message which should be calculated
\param      ucSize    - The message size
***********************************************************************************/
u32 OS_Communication_CreateMessageCrc(u8* pucBuffer, u8 ucSize)
{
    /* Start with an initial value */
    u32 ulCrc32 = CRC_INITIAL_VALUE;
    
    /* Calculate the CRC */
    ulCrc32 = OS_Crc32Buffer(pucBuffer, ucSize, ulCrc32);
    
    return ulCrc32;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       05.02.2019  
\brief      Creates a new message frame where the destination and source is chosable.
\return     ucCommand - The Command id
\param      sMsgFrame - The message frame
\param      ucDest    - The destination of this message
\param      ucSource  - The source address of this message 
***********************************************************************************/
u8 OS_Communication_CreateMessageSpecFrame(tsMessageFrame* psMsgFrame, u8 ucDest, u8 ucSource, u8* pucPayload, u8 ucPayloadLength)
{    
    /* Fill the header */
    psMsgFrame->sHeader.ucPreamble = PREAMBE;
    psMsgFrame->sHeader.ucDestAddress = ucDest;
    psMsgFrame->sHeader.ucSourceAddress = ucSource;
    psMsgFrame->sPayload.ucPayloadLen = ucPayloadLength;
    psMsgFrame->sPayload.pucData = pucPayload;
    
    /* Check if Query ID is already written (ACK) */
    if(psMsgFrame->sPayload.ucQueryID == INVALID_QUERY_ID)
    {
        psMsgFrame->sPayload.ucQueryID = ucGlobalQueryID;
   
        if(++ucGlobalQueryID >= INVALID_QUERY_ID)
        {
            ucGlobalQueryID = 0;
        }
    }
    
    u8 ucFrameSize = OS_Communication_GetFrameSize(psMsgFrame);
    
    return ucFrameSize;
}


//********************************************************************************
/*!
\author     KraemerE    
\date       05.02.2019  
\brief      Creates a new message frame.
\return     none
\param      sMsgFrame - The message frame
***********************************************************************************/
u8 OS_Communication_CreateMessageFrame(tsMessageFrame* psMsgFrame, u8* pucPayload, u8 ucPayloadLength)
{
    u8 ucFrameSize = 0;
    if(psMsgFrame)
    {
        #ifdef CY_PROJECT_NAME
            ucFrameSize = OS_Communication_CreateMessageSpecFrame(psMsgFrame, ADDRESS_SLAVE1, ADDRESS_MASTER, pucPayload, ucPayloadLength);
        #else
            ucFrameSize = OS_Communication_CreateMessageSpecFrame(psMsgFrame, ADDRESS_MASTER, ADDRESS_SLAVE1, pucPayload, ucPayloadLength);
        #endif
    }
    return ucFrameSize;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       10.02.2019  
\brief      Validates the messages by the source and destination address
\return     bValidAdress - Returns true when the destination and source is valid.
\param      sMsgFrame - The message frame
***********************************************************************************/
bool OS_Communication_ValidateMessageAddresses(const tsMessageFrame* psMsgFrame)
{
    bool bValidAdress = false;
    u8 ucSource = OS_Communication_GetSourceAddress(psMsgFrame);
    u8 ucDestination = OS_Communication_GetDestinationAddress(psMsgFrame);
    
    #ifdef CY_PROJECT_NAME
    if(ucSource == ADDRESS_SLAVE1 && ucDestination == ADDRESS_MASTER)
    #else
    if(ucSource == ADDRESS_MASTER && ucDestination == ADDRESS_SLAVE1)
    #endif
    {
        bValidAdress = true;
    }
    
    return bValidAdress;
}

//********************************************************************************
/*!
\author     KraemerE    
\date       17.02.2019  
\brief      Calculates the message size without the CRC
\return     u8 - Size of the message
\param      sMsgFrame - The message frame
***********************************************************************************/
u8 OS_Communication_GetMessageSizeWithoutCrc(const tsMessageFrame* psMsgFrame)
{
    u8 ucMessageSize = sizeof(*psMsgFrame) - sizeof(psMsgFrame->sPayload.pucData) + psMsgFrame->sPayload.ucPayloadLen - sizeof(psMsgFrame->ulCrc32);
    return ucMessageSize;    
}

//********************************************************************************
/*!
\author     KraemerE    
\date       07.07.2021  
\brief      Calculates the frame size
\return     u8 - Size of the message
\param      sMsgFrame - The message frame
***********************************************************************************/
u8 OS_Communication_GetFrameSize(const tsMessageFrame* psMsgFrame)
{
    /* To get the correct frame size the data pointer size has to be subtracted and the payload length instead added */
    u8 ucMessageSize = sizeof(*psMsgFrame) - sizeof(psMsgFrame->sPayload.pucData) + psMsgFrame->sPayload.ucPayloadLen;
    return ucMessageSize;    
}


//********************************************************************************
/*!
\author     KraemerE    
\date       11.04.2019  
\brief      This function handles the saved frames and resend them when the timeout
            has reached. When the retry counter is counted down to zero the message
            is discarded.
\return     none
\param      ucElapsedMs - The elapsed time in milliseconds since the last call
***********************************************************************************/
void OS_Communication_Tick(u8 ucElapsedMs)
{
    /* Check first if any message is in the saved buffer */
    if(sTransportCtrl.ucSavedMsgCounter)
    {             
        /* Decrement timeout */
        u8 ucBufferIdx;        
        for(ucBufferIdx = SAVED_MESSAGES; ucBufferIdx--;)
        {
            tsSavedMsgFrame* psSavedMsg = &sTransportCtrl.sSavedFrames[ucBufferIdx];
            
            /* Check for valid frame */
            if(psSavedMsg->ucQueryID != INVALID_QUERY_ID
                && psSavedMsg->ucReSendTimeout)
            {
                /* Check if subtraction is possible */
                if((s16)(psSavedMsg->ucReSendTimeout - ucElapsedMs) > 0)
                {
                    /* Decrement timeout by elapsed time */
                    psSavedMsg->ucReSendTimeout -= ucElapsedMs;
                }
                /* Timeout reached */
                else
                {
                     /***** Check if message shall be resend again *****/
                    if(psSavedMsg->ucReSendCounter)
                    {
                        /* Decrement retry counter */
                        --psSavedMsg->ucReSendCounter;
                        
                        /* Restart for next retry */
                        psSavedMsg->ucReSendTimeout = RESEND_TIMEOUT;
                                                
                        /* Get message from memory */
                        u8 aucMsgBuffer[DATA_MAX_SIZE];
                        OS_Memory_GetBuffer(psSavedMsg->pulMemoryAddress, &aucMsgBuffer[0], psSavedMsg->ucFrameLength);
                        
                        /* Re-Send message */
                        OS_Serial_UART_SendPacket((void*)&aucMsgBuffer[0], psSavedMsg->ucFrameLength);
                    }
                    /* Response still not received */
                    else
                    {          
                        ClearSavedMsgFrameEntry(psSavedMsg);
                    } 
                }
            }
        }
    }
}


//********************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      This function handles the received messages from the communication line.
            Calls another project specific Handler after the communication calculation
            was correct.
\return     none
\param      none
***********************************************************************************/
void OS_Communication_HandleSerialCommEvent(void)
{
    u8  aucRxBuffer[DATA_MAX_SIZE];
    u8  ucRxCount = sizeof(aucRxBuffer);
    u32 ulCalcCrc32 = CRC_INITIAL_VALUE;

    /* Clear buffer first */
    memset(&aucRxBuffer[0], 0, ucRxCount);

    /* Get message from the buffer */
    if(OS_Serial_UART_GetPacket(&aucRxBuffer[0], &ucRxCount))
    {
        /* Get the whole message first and cast it into the message frame */
        tsMessageFrame sMsgFrame;
        
        #if COPY_BUFFER_INTO_FRAME
            CreateMessageFrameFromByteStream(&aucRxBuffer[0], &sMsgFrame);
        #else
            LinkMessageFrameToByteStream(&aucRxBuffer[0], &sMsgFrame); 
        #endif

        /* Calculate the CRC for this message */
        ulCalcCrc32 = OS_Communication_CreateMessageCrc(&aucRxBuffer[0], OS_Communication_GetMessageSizeWithoutCrc(&sMsgFrame));

        /* Check for correct CRC */
        if(sMsgFrame.ulCrc32 == ulCalcCrc32)
        {
            /* Handle message */
            if(pFnMessageHandler)
            {  
                MessageHandler_HandleMessage(&sMsgFrame);
                //pFnMessageHandler(psMsgFrame);
            }
            else
            {
                OS_ErrorDebouncer_PutErrorInQueue(eInvalidPointerAccess);
                OS_Communication_SendDebugMessage("InvPtrAccess");
            }
        }
        //Somehow only query ID 0 leads to a CRC fault...
        else if(sMsgFrame.sPayload.ucQueryID != 0)
        {
            /* Put into error queue */
            char message[54];
            sprintf(message, "Invalid CRC (Query ID - %i) %ju != %ju", sMsgFrame.sPayload.ucQueryID,(uintmax_t)ulCalcCrc32, (uintmax_t)sMsgFrame.ulCrc32);
            OS_Communication_SendDebugMessage(message);
            OS_ErrorDebouncer_PutErrorInQueue(eMessageCrcFault);
        }
    }
}

#ifdef MOCK_HANDLEMESSAGE
void MessageHandler_HandleMessage(void* pvMsg)
{
    //MOCK functon
}
#endif

#endif //#USE_OS_COMMUNICATION